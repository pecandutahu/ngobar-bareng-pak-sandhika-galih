function getListSurat() {
    $.ajax({
        url: 'https://al-quran-8d642.firebaseio.com/data.json?print=pretty',

        success: function (result) {

            $.each(result, function (i, data) {
                $('#list-surat').append(`
                    <a href="#" class="list-group-item" id="surat" data-id="` + data.nomor + `">` + data.nomor + `.   ` + data.nama + ` ` + data.asma + `</a>
                `);


                $('#list-surat').on('click', '#surat', function () {
                    if ($(this).data('id') == data.nomor) {
                        $('#detail-surat').html(`
                            <h3 class="card-title">` + data.nama + `</h3>
                            <h4>` + data.type + `</h4>
                                <p class="card-text">` + data.keterangan + `</p>
                            <span class="text-warning">
                            Jumlah ayat:  ` + data.ayat + ` ayat
                            </span>
                        `);
                    }

                });

            });
        }
    });
}


function getListayat() {
    $('#list-surat').on('click', '#surat', function () {
        $.ajax({
            url: "https://al-quran-8d642.firebaseio.com/surat/" + $(this).data('id') + ".json",
            success: function (ayat) {
                console.log(ayat);
                $.each(ayat, function (i, data) {
                    $('#ayat').append(`
                        <p>` + data.ar + `</p>
                        <small class="terjemahan">` + data.id + `</small>
                        <hr>
                    `);
                });
            }
        });
    });
}

getListSurat();
getListayat();